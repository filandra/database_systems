// Created new database
use m181_Filandr

// Created new collections
db.createCollection("books")
db.createCollection("authors")

// Create reference object ids
Dostoevsky = ObjectId()
Orwell = ObjectId()

// Fill collection books with data - use reference for author and index for published year
print("Fill data")
db.books.insert( { _id: "Crime and Punishment", published: 1867, author_id: Dostoevsky, genre: ["philosophical", "drama"] } ) 
db.books.insert( { _id: "The Brothers Karamazov", published: 1880, author_id: Dostoevsky, genre: ["psychological", "drama"] } ) 

db.books.insert( { _id: "Animal Farm", author_id: Orwell, published: 1945, genre: ["political", "satire"] } ) 
db.books.insert( { _id: "1984", author_id: Orwell, published: 1949, genre: ["political", "dystopian","drama"] } ) 

db.books.ensureIndex( { "published": 1 } )

// Fill collection authors with data - use embedded data for information about author
db.authors.insert( { _id: Orwell, name: "Orwell", info: { born: 1903, language: "English" } } )
db.authors.insert( { _id: Dostoevsky, name: "Dostoevsky", info: { born: 1821, language: "Russian" } } )
print()

// Retrieve the data
print("Find all satire books")
db.books.find( { genre: "satire" } )
print()

print("Find all philosophical or psychological books")
db.books.find( { genre: { $in: [ 'philosophical', 'psychological' ] } } )
print()

// Retrieve data using indexes
print("Find all books published earlier than 1870 using indexes")
db.books.find( { published : {"$lt" : 1870} } )
print()

print("Find all books published later than 1945 using indexes")
db.books.find( { published : {"$gt" : 1945} } )
print()

print("All books sorted in descending order")
db.books.find( { published : {"$gt" : 1800} } ).sort( { published: -1 } )
print()

// Use references
print("Use reference from author Orwell to find his books")
db.books.find( { author_id: db.authors.find( { name: "Orwell" } )[0]._id } )
print()

print("Use reference from book Crime and Punishment to find the author")
db.authors.find( { _id: db.books.find( { _id: "Crime and Punishment" } )[0].author_id } )
print()

// Clean up
print("Drop the tables")
db.books.drop()
db.authors.drop()