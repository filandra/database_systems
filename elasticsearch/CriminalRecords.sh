# Create new index
curl -X PUT localhost:9200/m181_filandr_criminals?pretty

# Load data into index
curl -H "Content-Type: application/json" -XPOST "localhost:9200/m181_filandr_criminals/_doc/_bulk?pretty&refresh" --data-binary "@/home/m181_Filandr/assignments/elasticsearch/records.json"

# Add new criminal record
curl -X PUT "localhost:9200/m181_filandr_criminals/_doc/8?pretty" -H 'Content-Type: application/json' -d'
{
	"firstname":"Arthas","lastname":"Menethil","age":24,"gender":"M","crime":["treason", "war crimes"],"security":"Highest", "years":1000
}
'

# Update old criminal record
curl -X POST "localhost:9200/m181_filandr_criminals/_doc/5/_update?pretty" -H 'Content-Type: application/json' -d'
{
	"doc": {"years":15}
}
'

# Read the updated record
curl -X GET "localhost:9200/m181_filandr_criminals/_doc/5?pretty"

# Sort criminal records by age of perpetrators
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} },
  "sort": { "age": { "order": "desc" } }
}
'

# Search for murder keyword in crimes and firstnames
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
    "query": {
        "multi_match" : {
            "query" : "murder",
            "fields" : ["crime", "firstname"]
        }
    },
    "_source": ["firstname", "lastname", "crime"]
}
'

# Find all perpetrators with medium security
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match": { "security": "Medium" } }
}
'

# Select all criminals whose first name starts with r
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
    "query": {
        "regexp" : {
            "firstname" : "r[a-z]*"
        }
    },
    "_source": ["firstname", "lastname"]
}
'

# List all crimes by gender
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} },
  "_source": ["gender", "crime"]
}
'

# Find criminal record of Doom Guy
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match": { "firstname": "Doom" } }
}
'

# Group criminals by their security
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
  "size": 0,
  "aggs": {
    "group_by_security": {
      "terms": {
        "field": "security.keyword"       
      }
    }
  }
}
'

# Find crimes which earn you between 10 and 20 years in prison
curl -X GET "localhost:9200/m181_filandr_criminals/_search?pretty" -H 'Content-Type: application/json' -d'
{
    "query": {
        "range" : {
            "years": {
                "gte": "10",
                "lte": "30"
            }
        }
    },
    "_source" : ["years", "crime"]
}
'

# Drop the data
curl -X DELETE localhost:9200/m181_filandr_criminals?pretty