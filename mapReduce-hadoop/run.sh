#!/bin/sh

hadoop fs -rm /user/m181_Filandr/PopulationCount/input1/population.txt
hadoop fs -copyFromLocal population.txt /user/m181_Filandr/PopulationCount/input1

javac -classpath /home/NOSQL/MFF-NDBI040/mapreduce/hadoop-common-3.1.1.jar:/home/NOSQL/MFF-NDBI040/mapreduce/hadoop-mapreduce-client-core-3.1.1.jar -d classes/ PopulationCount.java
jar -cvf PopulationCount.jar -C classes/ .

rm -f result.txt
hadoop jar PopulationCount.jar PopulationCount /user/m181_Filandr/PopulationCount/input1 /user/m181_Filandr/PopulationCount/output1
hadoop fs -copyToLocal /user/m181_Filandr/PopulationCount/output1/part-r-00000 result.txt

printf "\n"
cat result.txt