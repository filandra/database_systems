import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class PopulationCount {

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);    
    private Text word = new Text();
    private String city = "Praha";
    private int age = 18;
    private Text hit = new Text("Population from Prague over 18:");
    private Text total = new Text("Total population:");

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      boolean fromCity = false;
      boolean overAge = false;

      // Go through every line
      for (String line : value.toString().split(";")) {

        fromCity = false;
        overAge = false;

        // Go through each word
        for (String word : line.split("\\s+")) {      

          // Check if the person is from given city
          if (word.equals(city)) {          
            fromCity = true;
          }
          
          // Check if the person is over given age
          try{
             if (Integer.parseInt(word) >= age) {          
              overAge = true;
            }  
          }catch(Exception e){}           
        }

        // Register population and hits
        context.write(total, one);
        if (fromCity && overAge) {
          context.write(hit, one);
        }                 
      }
    }
  }

  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int counter = 0;      

      for (IntWritable val : values) {
        counter += val.get();
      }      
      result.set((int)counter);

      context.write(key, result);        
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "percentage count");
    job.setJarByClass(PopulationCount.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    Path output_path = new Path(args[1]);
    FileOutputFormat.setOutputPath(job, output_path);
    
    if (output_path.getFileSystem(conf).exists(output_path))
      output_path.getFileSystem(conf).delete(output_path, true);

    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
