#Author Adam Filandr
# 24.11.2018

# Add users
curl -X PUT -H "Content-Type: application/json" \
 -H 'Link: </riak/m181_Filandr_users/Mat>; riaktag="friendship", </riak/m181_Filandr_users/Patylda>; riaktag="relationship"' \
 -d '{"name":"Pat", "location":"Horni dolni"}' http://localhost:10011/riak/m181_Filandr_users/Pat

curl -X PUT -H "Content-Type: application/json" \
 -H 'Link: </riak/m181_Filandr_users/Pat>; riaktag="friendship", </riak/m181_Filandr_users/Matylda>; riaktag="relationship"' \
 -d '{"name":"Mat", "location":"Horni dolni"}' http://localhost:10011/riak/m181_Filandr_users/Mat

curl -X PUT -H "Content-Type: application/json" \
 -H 'Link: </riak/m181_Filandr_users/Matylda>; riaktag="friendship", </riak/m181_Filandr_users/Pat>; riaktag="relationship"' \
 -d '{"name":"Patylda", "location":"Dolni horni"}' http://localhost:10011/riak/m181_Filandr_users/Patylda

curl -X PUT -H "Content-Type: application/json" \
 -H 'Link: </riak/m181_Filandr_users/Patylda>; riaktag="friendship", </riak/m181_Filandr_users/Mat>; riaktag="relationship"' \
 -d '{"name":"Matylda", "location":"Dolni horni"}' http://localhost:10011/riak/m181_Filandr_users/Matylda

 curl http://localhost:10011/riak/m181_Filandr_users/Pat
 echo
 curl http://localhost:10011/riak/m181_Filandr_users/Mat
 echo
 curl http://localhost:10011/riak/m181_Filandr_users/Patylda
 echo
 curl http://localhost:10011/riak/m181_Filandr_users/Matylda
 echo

 sleep 2

# Usage of the site
curl -H "Content-Type: plain/text" -d "Pat: Afternoon beer and football... ou yeeea" http://localhost:10011/riak/m181_Filandr_lastPost/Pat
curl http://localhost:10011/riak/m181_Filandr_lastPost/Pat && echo && sleep 2

curl -H "Content-Type: plain/text" -d "Patylda: Husby is being lazy again :(" http://localhost:10011/riak/m181_Filandr_lastPost/Patylda
curl http://localhost:10011/riak/m181_Filandr_lastPost/Patylda && echo && sleep 2

curl -H "Content-Type: plain/text" -d "Mat: Guys I need to lend me some money... and also some painkillers" http://localhost:10011/riak/m181_Filandr_lastPost/Mat
curl http://localhost:10011/riak/m181_Filandr_lastPost/Mat && echo && sleep 2

curl -H "Content-Type: plain/text" -d "Matylda: #newclothes #newring #newcar #newdog #richlife" http://localhost:10011/riak/m181_Filandr_lastPost/Matylda
curl http://localhost:10011/riak/m181_Filandr_lastPost/Matylda && echo && sleep 2

curl -H "Content-Type: plain/text" -d "Pat: come on honey, I need to rest after the hard work..." http://localhost:10011/riak/m181_Filandr_lastPost/Pat
curl http://localhost:10011/riak/m181_Filandr_lastPost/Pat && echo && sleep 2

curl -H "Content-Type: plain/text" -d "Patylda: You are unemployed!!" http://localhost:10011/riak/m181_Filandr_lastPost/Patylda
curl http://localhost:10011/riak/m181_Filandr_lastPost/Patylda && echo && sleep 2

curl -H "Content-Type: plain/text" -d "Patylda: I divorce you! ;(" http://localhost:10011/riak/m181_Filandr_lastPost/Patylda
curl http://localhost:10011/riak/m181_Filandr_lastPost/Patylda && echo && sleep 2

curl -X PUT -H "Content-Type: application/json" \
 -H 'Link: </riak/m181_Filandr_users/Matylda>; riaktag="friendship"' \
 -d '{"name":"Patylda", "location":"Dolni horni"}' http://localhost:10011/riak/m181_Filandr_users/Patylda

sleep 2

curl -H "Content-Type: plain/text" -d "Pat: HONEY!!!" http://localhost:10011/riak/m181_Filandr_lastPost/Pat
curl http://localhost:10011/riak/m181_Filandr_lastPost/Pat && echo && sleep 2

# Pat searches connections of his wife in desparation (link walk)
curl -i http://localhost:10011/riak/m181_Filandr_users/Patylda/_,_,_

sleep 4

curl -H "Content-Type: plain/text" -d "Pat: She really left... ;( Good bye cruel world ;(" http://localhost:10011/riak/m181_Filandr_lastPost/Pat
curl http://localhost:10011/riak/m181_Filandr_lastPost/Pat && echo && sleep 2

# Pat "deletes" himself from the "social network" (rated pg 13)
curl -i -X DELETE http://localhost:10011/riak/m181_Filandr_users/Pat

sleep 2

curl -H "Content-Type: plain/text" -d "Mat: You alring bro?" http://localhost:10011/riak/m181_Filandr_lastPost/Mat
curl http://localhost:10011/riak/m181_Filandr_lastPost/Mat && echo && sleep 2

# Mat is searching for his life long friend
curl -i http://localhost:10011/riak/m181_Filandr_users/Mat/m181_Filandr_users,friendship,1
echo Pat && curl http://localhost:10011/riak/m181_Filandr_users/Pat

sleep 4

curl -H "Content-Type: plain/text" -d "Mat: #sad" http://localhost:10011/riak/m181_Filandr_lastPost/Mat
curl http://localhost:10011/riak/m181_Filandr_lastPost/Mat && echo && sleep 2

curl -H "Content-Type: plain/text" -d "Matylda: Mat stop crying, this is the fifth time this happened..." http://localhost:10011/riak/m181_Filandr_lastPost/Matylda
curl http://localhost:10011/riak/m181_Filandr_lastPost/Matylda && echo && sleep 2
