r="redis-cli -n 33"

echo -------------------------------------------------------
echo "clean up"

$r DEL balance

$r DEL products:Bitcoin
$r DEL products:Tesla
$r DEL products:KFC
$r DEL products:LabMeat

$r DEL category:tech
$r DEL category:food
$r DEL category:disrupting

$r DEL prices

$r DEL operations

echo -------------------------------------------------------
echo "inicialization"
# Users balance
$r SET balance 1000

# Information about products (Hash utilization)
$r HMSET products:Bitcoin tech disrupting popular getRichQuickDummy
$r HMSET products:Tesla tech automobile
$r HMSET products:KFC food controversial brand established
$r HMSET products:LabMeat food disrupting experimental new

# Categories of products (Unordered set utilization)

$r SADD category:tech Bitcoin Tesla
$r SADD category:food KFC LabMeat
$r SADD category:disrupting Bitcoin LabMeat

# Stock prices (Ordered set utilization)
$r ZADD prices 500 Bitcoin 100 Tesla 150 KFC 40 LabMeat

echo ------------------------------------------------------- && sleep 2
# Usage of the stock market

# User searches through the market
echo "I wonder what new technology is trading" && sleep 2
$r SMEMBERS category:tech
echo "Maybe some of this stuff is also marked as disrupting" && sleep 2
$r SINTER category:tech category:disrupting
echo "What is this bitcoin thing anyway??" && sleep 2
$r HGETALL products:Bitcoin 
echo "How much does it cost?" && sleep 3
$r ZSCORE prices Bitcoin
echo "Well it looks like it is the most expensive one.. it must be popular" && sleep 3
$r ZRANGE prices 0 -1
echo "Well gonna buy some" && sleep 3

# User decides to buy bitcoins
# Stack of operaitions to perform (List utilization)
$r LPUSH operations buyBitcoin
$r LPUSH operations buyBitcoin

# System consumes the operations
$r RPOP operations
$r SET balance 500
echo "balance :"
$r GET balance && sleep 2

$r RPOP operations
$r SET balance 0
echo "balance :"
$r GET balance && sleep 2

echo "The system: Bitcoin bubble bursts - current prices is...."
$r ZINCRBY prices -499 Bitcoin && sleep 2

echo "No regrets"